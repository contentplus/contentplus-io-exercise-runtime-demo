import $ from 'jquery';

export default class ModulePreload {
    handleLoadingStart(container, options) {
        $(container)
            .parent()
            .addClass('module')
            .addClass('module--loading');

        $(container)
            .parent()
            .children('.module-preloader').remove();

        let preloader = $('<div class="module-preloader">' +
            '<div>Trwa wczytywanie danych<span class="dot1">.</span><span class="dot2">.</span><span\n' +
            '            class="dot3">.</span></div>' +
            '</div>');

        this._applyOptions(preloader, options)

        $(container)
            .parent()
            .append(preloader);
    }

    handleLoadingStop(container, options) {
        $(container)
            .parent()
            .removeClass('module--loading');

        $(container)
            .parent()
            .children('.module-preloader').remove();
    }

    handleLoadingError(error, container, options) {
        $(container)
            .parent()
            .removeClass('module--loading');

        $(container)
            .parent()
            .children('.module-preloader').remove();

        let preloader = $('<div class="module-preloader">' +
            '<div>Wystąpił błąd</div>' +
            '</div>');

        preloader.addClass('module-preloader--has-error');
        this._applyOptions(preloader, options)

        $(container)
            .parent()
            .append(preloader);
    }

    _applyOptions(preloader, options) {
        if (options) {
            if (options.dark) {
                preloader.addClass('module-preloader--dark');
            }
            if (options.style) {
                preloader.addClass('module-preloader--style-' + (options.style + ''));
            }
        }
    }
}
