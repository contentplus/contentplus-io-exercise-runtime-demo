export default class ExerciseStorageSaveProtector {
    wrapped;
    counter = 0;

    constructor(wrapped) {
        this.wrapped = wrapped;

        this._checkCallback = (event) => {
            event.preventDefault();
            return event.returnValue = "Nie zapisano stanu ćwiczeń. Możesz utracić wyniki";
        };
    }

    _onStart() {
        if (0 === this.counter) {
            window.addEventListener('beforeunload', this._checkCallback);
        }
        this.counter++;
    }

    _onStop() {
        this.counter--;
        if (0 === this.counter) {
            window.removeEventListener('beforeunload', this._checkCallback);
        }
    }

    async store(key, state, result) {
        this._onStart();
        try {
            return await this.wrapped.store(key, state, result);
        } finally {
            this._onStop();
        }
    }

    fetchState(key) {
        return this.wrapped.fetchState(key);
    }

    async uploadUserFile(key, fileId, file) {
        this._onStart();
        try {
            return await this.wrapped.uploadUserFile(key, fileId, file);
        } finally {
            this._onStop();
        }
    }

    getUserFileUrl(key, fileId) {
        return this.wrapped.getUserFileUrl(key, fileId);
    }

    canUploadUserFile() {
        return this.wrapped.canUploadUserFile();
    }
}

