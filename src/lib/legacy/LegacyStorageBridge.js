export default class LegacyStorageBridge {
    dataStorage;
    exerciseResultCollector;

    constructor(dataStorage, exerciseResultCollector) {
        this.dataStorage = dataStorage;
        this.exerciseResultCollector = exerciseResultCollector;
    }

    fetchState(key) {
        return new Promise(resolve => {
            if (!this.dataStorage) {
                resolve(null);
                return;
            }

            this.dataStorage.load(key, (value) => {
                resolve(value);
            })
        });
    }

    store(key, state, result) {
        let promises = [];

        if (this.dataStorage) {
            promises.push(new Promise(resolve => {
                this.dataStorage.store(key, state, () => {
                    resolve();
                })
            }))
        }

        if (this.exerciseResultCollector && result) {
            promises.push(new Promise(resolve => {
                let isCorrect = result.correct === result.total;

                this.exerciseResultCollector.store(key, isCorrect, () => {
                    resolve();
                })
            }))
        }

        return Promise.all(promises);
    }

    canUploadUserFile() {
        return false;
    }
}