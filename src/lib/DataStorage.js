export default class DataStorage {
    fetchState(key) {
        return new Promise(resolve => {
            let value = null;
            try {
                let jsonContent = localStorage.getItem(key);
                value = JSON.parse(jsonContent);
            } catch (e) {
            }
            resolve(value);
        });
    }

    store(key, state, result) {
        return new Promise(resolve => {
            localStorage.setItem(
                key,
                JSON.stringify(state)
            );
            resolve();
        });
    }

    canUploadUserFile() {
        return false;
    }
}
