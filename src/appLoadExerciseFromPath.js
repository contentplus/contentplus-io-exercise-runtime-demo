import {amdModuleLoader, ExerciseApi, ModuleLoader} from './lib/exercise-runtime'
import ModulePreload from "./lib/ModulePreload";
import DataStorage from "./lib/DataStorage";
import EventBus from "./lib/EventBus";
import axios from 'axios';
import './style/main.scss'

import $ from 'jquery';
import 'jquery-ui-bundle';

window.$ = window.jQuery = $;

amdModuleLoader.registerInterceptor(function (context, moduleName, url) {
    let result = (function (moduleName) {
        switch (moduleName) {
            case 'jqueryui':
                return Promise.resolve(null);
            case 'amd-module-loader':
                return Promise.resolve(amdModuleLoader);
            case 'vue':
                return import('vue');
            case 'vue-router':
                return import('vue-router');
            case 'vuex':
                return import('vuex');
            case 'jquery':
            case 'jquery:2':
                return import('jquery');
            case 'underscore':
                return import('underscore');
            case 'backbone':
                return import('backbone');
            case 'react':
            case 'react:16':
                return import('react');
            case 'axios':
                return import('axios');
        }
    })(moduleName);

    if (result) {
        console.debug('Loading module', moduleName);
        return result;
    }
});

let storage = new DataStorage(),
    eventBus = new EventBus(),
    exerciseApiOptions = {
        manifestResolver: (code, Axios) => {
            let host = this.options.projectHost || '';
            let path = CpString.rtrim(host, '/') + '/resource/' + code + '/manifest';
            return Axios.get(path).then(response => response.data);
        }
    };

let apiFactory = (module) => {
    return new ExerciseApi(
        module,
        storage,
        eventBus,
        moduleLoader,
        exerciseApiOptions
    )
};

let moduleLoader = new ModuleLoader(apiFactory, new ModulePreload());
moduleLoader.register((engine) => {
    let enginePath = '/engine/' + engine;
    return axios
        .get(enginePath + '/engine.json')
        .then((response) => {
            return {
                baseUrl: enginePath + '/',
                manifest: response.data
            };
        })
});

let storageId = 'demo2';

function getContrastModeName(value) {
    switch (value) {
        case 1:
            return 'yellowOnBlack';
        case 2:
            return 'blackOnYellow';
        case 3:
            return 'whiteOnBlack';
        default:
            return value
    }
}

let dataPath = '/demo/1/';

axios.get(dataPath + 'manifest.json')
    .then((response) => {
        let manifest = response.data;

        moduleLoader.initModule(
            manifest,
            dataPath,
            document.getElementById('app'),
            Object.assign({
                locale: 'pl',
                contrastMode: getContrastModeName(0),
                showAnswers: true,
                isLoggedIn: false,
                generatingPrintPdf: false
            }),
            storageId
        ).then((module) => {
            console.log('Module has been loaded', module);
            module.resize();
        });
    });
