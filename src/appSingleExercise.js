import {amdModuleLoader, ExerciseApi, ModuleLoader} from './lib/exercise-runtime'
import ModulePreload from "./lib/ModulePreload";
import DataStorage from "./lib/DataStorage";
import EventBus from "./lib/EventBus";
import axios from 'axios';
import './style/main.scss'

import $ from 'jquery';
import 'jquery-ui-bundle';

window.$ = window.jQuery = $;

amdModuleLoader.registerInterceptor(function (context, moduleName, url) {
    let result = (function (moduleName) {
        switch (moduleName) {
            case 'jqueryui':
                return Promise.resolve(null);
            case 'amd-module-loader':
                return Promise.resolve(amdModuleLoader);
            case 'vue':
                return import('vue');
            case 'vue-router':
                return import('vue-router');
            case 'vuex':
                return import('vuex');
            case 'jquery':
            case 'jquery:2':
                return import('jquery');
            case 'underscore':
                return import('underscore');
            case 'backbone':
                return import('backbone');
            case 'react':
            case 'react:16':
                return import('react');
            case 'axios':
                return import('axios');
        }
    })(moduleName);

    if (result) {
        console.debug('Loading module', moduleName);
        return result;
    }
});

let storage = new DataStorage(),
    eventBus = new EventBus(),
    exerciseApiOptions = {
        manifestResolver: (code, Axios) => {
            let host = this.options.projectHost || '';
            let path = CpString.rtrim(host, '/') + '/resource/' + code + '/manifest';
            return Axios.get(path).then(response => response.data);
        }
    };

let apiFactory = (module) => {
    return new ExerciseApi(
        module,
        storage,
        eventBus,
        moduleLoader,
        exerciseApiOptions
    )
};

let moduleLoader = new ModuleLoader(apiFactory, new ModulePreload());
moduleLoader.register((engine) => {
    let enginePath = '/engine/' + engine;
    return axios
        .get(enginePath + '/engine.json')
        .then((response) => {
            return {
                baseUrl: enginePath + '/',
                manifest: response.data
            };
        })
});

let manifest = {
    "caption" : "<div class=\"figure_caption_bibliography\" data-editor-no-parse><span lang=\"pl\"><span class=\"ref--before\"> \u0179r\u00f3d\u0142o: <\/span>Jan  Nowak. <\/span><\/div>",
    "data": {
        "code": null,
        "mode": null,
        "type": "Q03XofY",
        "content": {
            "items": {
                "elements": [{
                    "text": "wzr\u00f3s\u0142 z\u00a01% do 4%.",
                    "correct": false
                }, {
                    "text": "wzr\u00f3s\u0142 z\u00a04% do 12%.",
                    "correct": false
                }, {"text": "spad\u0142 z\u00a012% do 4%.", "correct": true}, {
                    "text": "spad\u0142 z\u00a015% do 8%.",
                    "correct": false
                }]
            },
            "title": "Wybierz poprawne zako\u0144czenie zdania.<br\/>W wyniku wojny domowej z\u00a0lat 1991\u20131995 odsetek Serb\u00f3w \u017cyj\u0105cych w\u00a0Chorwacji\u2026",
            "gameTitle": "Wybierz poprawne zako\u0144czenie zdania.<br\/>W wyniku wojny domowej z\u00a0lat 1991\u20131995 odsetek Serb\u00f3w \u017cyj\u0105cych w\u00a0Chorwacji\u2026",
            "gameSubtitle": ""
        },
        "options": {
            "theme": null,
            "isTest": true,
            "footerStyle": "extended",
            "isFullScreen": false,
            "isAnswerCaseSensitive": true
        },
        "shuffle": true,
        "resources": [],
        "resourcesCount": {"audio": 0, "image": 0, "video": 0},
        "feedbackNegative": {"title": "Niepoprawna odpowied\u017a.", "content": ""},
        "feedbackPositive": {"title": "Poprawna odpowied\u017a.", "content": ""}
    }, "files": [], "engine": "core\/Q03XofY"
};

let dataPath = null,
    storageId = 'demo1';

function getContrastModeName(value) {
    switch (value) {
        case 1:
            return 'yellowOnBlack';
        case 2:
            return 'blackOnYellow';
        case 3:
            return 'whiteOnBlack';
        default:
            return value
    }
}

moduleLoader.initModule(
    manifest,
    dataPath,
    document.getElementById('app'),
    Object.assign({
        locale: 'pl',
        contrastMode: getContrastModeName(0),
        showAnswers: true,
        isLoggedIn: false,
        generatingPrintPdf: false
    }),
    storageId
).then((module) => {
    console.log('Module has been loaded', module);
    module.resize();
});
